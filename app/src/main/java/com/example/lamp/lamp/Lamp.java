package com.example.lamp.lamp;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;


public class Lamp extends Activity {

    ImageButton image;
    Button buttonOn;
    Button buttonOff;
    Context context;
    Camera camera;
    private boolean hasLED;
    Camera.Parameters params;

    private boolean Lamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lamp);
        context = this;


        image = (ImageButton) findViewById(R.id.imageButton);

        try {
            camera = Camera.open();
            params = camera.getParameters();
        } catch (RuntimeException e) {
            Log.e("Problemy z aparatem ", e.getMessage());
        }

        Lamp = false;
        tapeta();

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Lamp){
                    lampOFF();
                }
                else{
                   lampON();
                }
            }
        });

    }

    private boolean has_LED(){

        hasLED = getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        if(!hasLED) {
            Toast tn = Toast.makeText(context, "Nie posiadasz LED", Toast.LENGTH_SHORT);
            tn.show();
            return false;
        } else{
            return true;
        }
    }

    private void lampON(){
            if(!Lamp){
                params = camera.getParameters();
                params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                camera.setParameters(params);
                camera.startPreview();
                Lamp = true;
                tapeta();
            }
    }

    private void lampOFF(){
        if(Lamp){
            params = camera.getParameters();
            params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(params);
            camera.stopPreview();
            Lamp = false;
            tapeta();
        }
    }

    private void tapeta(){
        if(Lamp){

            image.setImageResource(R.drawable.lighton1);

        } else{

            image.setImageResource(R.drawable.lightof1);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.lamp, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
